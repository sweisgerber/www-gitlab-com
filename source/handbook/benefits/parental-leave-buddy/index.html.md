---
layout: markdown_page
title: "Parental Leave and Return Tool Kit for Managers and Team Members"
---

## On This Page
On this you will be provide tools to support parents before, during and after maternity leave, shared parental leave and
adoption leave, etc.  

 

## Manager Tool Kit
Do you have a team member leaving and want to ensure you understand how to support?  There are so many things to consider when we think about parental leave as a manager. This toolkit is for everyone who is managing someone who is expecting to become, or has recently become, a parent. 

### To Do List
* Review our [parental leave benefits/policy](https://about.gitlab.com/handbook/benefits/#parental-leave)      
* **Congratulate your team member** This is a time when many parents report feeling vulnerable, so your support will make all the difference.  Suggested things to say when you are told the news: Congratulations, How are you feeling?, How can we support you?, Do you know how to access the relevant maternity/shared parental/adoption policy?, Is the pregnancy/adoption confidential for now?
* **Virtual Baby (or Growing Your Family) Shower ** Although we are fully distributed, there are many ways we can celebrate with our team members as people who work in office.  One of those ways could be a virtual baby shower. Additional details can be found below.
* **Arrange Out of Office Coverage** This may vary for work functions (i.e sales, etc) Consider what happens if the person needs to leave earlier than planned due to medical issues.

#### Virtual Baby (or Growing Your Family) Shower 
A nice way to celebrate a team member's new addition to their family is by hosting a virtual baby/growing your family shower. In colocated companies, this is something that teams often do to wish a team member well as they head out on parental or adoption leave. We encourage virtual showers to be considered for all team members (not just expectant moms). In order to help teams host a virtual shower, here are some resources that can help you get started: 
1. Ask the team member if they would be comfortable with a virtual shower. In a remote environment, surprising the parent-to-be is less effective as they need to be an active participant in the process. 
1. If the team member is comfortable and open to an event, ask them who they would like to include in the celebration and if they have any gift registries that other team members can buy gifts from so they are purchasing items that the baby will need. The purchase of gifts should always be optional. 
1. Send out an invite in advance of the event so attendees can plan to be available for that date and time. 
1. Create an agenda for the event. Be mindful to create an inclusive and welcoming atmosphere. Consider running potential games by the celebrated team member to ensure comfortability. See a sample agenda below:
* Kick off the meeting by thanking everyone for coming, review the agenda and how games and prizes will be work.  
* Start meeting off with opening ice breaker: An option is a Baby MadLibs game.
* Game 1 - Measure the belly. If the soon-to-be parent is an expecting mom, you can play this game. It requires a roll of toliet paper. Participants guess how many sheets of toliet paper it will take to wrap around the expecting mom's belly. The closest guess gets a prize. 
* Open presents - Have the soon-to-be parent save the presents shipped to them to be opened during the celebration. 
* Game 2 - Songs with the word "Baby". Have participants write as many songs as they can think of that have the word "baby" in them. Whoever thinks of the most songs within three minutes, wins a prize.  
* Close the event with an open forum for people to ask fun questions and send positive affirmations.


## Team Member Tool Kit
* Review our [parental leave benefits/policy](https://about.gitlab.com/handbook/benefits/#parental-leave)
* Slack channel to connect with other parents `#intheparenthood`.
* Read the handbook section on [returning from parental leave](https://about.gitlab.com/handbook/paid-time-off/#returning-from-work-after-parental-leave) with tips for the transition back to work
* Consider reaching out to a [Parental Leave Re-Entry Buddy](https://about.gitlab.com/handbook/paid-time-off/#parental-leave-reentry-buddies)

