---
layout: handbook-page-toc
title: "Competencies"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

GitLab has competencies as a common framework to learn things.
The competencies include both general and role specific compentencies.
Competencies are useful to have a [Single Source of Truth (SSoT)](https://docs.gitlab.com/ee/development/documentation/styleguide.html#why-a-single-source-of-truth) framework for things we need team members to learn.

## Principles

1. We will re-use the same materials for different audiences by having them on a competency page.
1. We will make competencies accessible to everyone in the world, including doing the test and receiving the certification (via Google forms and Zapier)
1. We wil work handbook first so [everyone can contribute](https://about.gitlab.com/company/strategy/#mission)
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oXTZQpICxeE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Usage

The following initiatives should use the same competencies as their SSoT.
Instead of maintaining separate materials they should link back to the most relevant place where the competencie is defined. For general competencies this is likely our [values](/handbook/values) page. For role specific competencies this is likely the [job-family](/handbook/hiring/job-families/#format) page.
For example, we should have one SSoT for how to articulate the value of GitLab.

1.  [Job family requirements](/handbook/hiring/job-families/#format)
1.  [Interview scoring](/handbook/hiring/recruiting-framework/hiring-manager/#step-12hm-complete-feedback-in-greenhousenext-steps)
1.  [Promotion criteria](/handbook/people-group/promotions-transfers/)
1.  [9 box assessments](https://www.predictivesuccess.com/blog/9-box/)
1.  [Performance/Potential criteria](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix)
1.  [Succession planning](/handbook/people-group/performance-assessments-and-succession-planning/#succession-planning)
1.  [Learning and development](/handbook/people-group/learning-and-development/)
1.  [PDPs/PIPs](/handbook/underperformance/)
1.  [Career development](/handbook/people-group/learning-and-development/career-development/)
1.  [360 reviews](/handbook/people-group/360-feedback/)
1.  [Sales training](/handbook/sales/training/)
1.  [Sales enablement sessions](/handbook/sales/training/sales-enablement-sessions/)
1.  [Field enablement](/handbook/sales/field-operations/field-enablement/)
1.  [GitLab Training tracks](/training/)
1.  [GitLab University](https://docs.gitlab.com/ee/university/)
1.  [Customer Success Skills Exchange Sessions](/handbook/sales/training/customer-success-skills-exchange/)
1.  [Professional services offerings](/handbook/customer-success/professional-services-engineering/offerings/)
1.  [Onboarding](/handbook/general-onboarding/) both general and department specific
1.  [Reseller onboarding](/handbook/resellers/onboarding/)
1.  Learn@ GitLab - mentioned Strategy GC March 3
1.  Pathfactory flows - mentioned Strategy GC March 3
1.  [Training materials](/handbook/training/) This is a SSOT page where all our existing training materials are linked and easily found. As the page is updated with the trainings listed above, they will be removed from this list 

## Content

1. Content is in the relevant section of our handbook (with embedded videos and pictures)
1. Tests are created in Google Forms (via Zapier [you get a certification](/handbook/people-group/learning-and-development/certifications/#how-to-create-a-certification))
1. The [leadership forum](/handbook/people-group/learning-and-development/leadership-forum/) is organized by L&D
1. Maybe we can also do about 5 questions (with example of a good answer/level) per level

## Levels
Competencies are defined based-on job-level. For example, the definition of competency in the GitLab value of iteration and how you demonstrate that competency are different whether you are an Intermediate Product Manager or a EVP of Product.

In general, your scope of impact of a competency should exceed your span of management control.

| Level | Scope of Impact | Expected Behaviors | 
|-----------------|----------------------------------------|------------------------------------------|
| Associate | Own work | Learns/Develops | 
| Intermediate | Work within team | Grows/Acts | 
| Senior | Cross functional work | Models | 
| Staff/Manager | Across Teams | Implements | 
| Senior Manager | Across Sub-Departments | Fosters | 
| Director | Across Departments | Drives the framework, strategy and plans | 
| Senior Director | Across Divisions | Develops the framework and strategy | 
| VP | Across Company + External Stakeholders | Leads Changes |
| EVP/CXO | Across Company + External Stakeholders | Champions  | 

## List

Take this [quiz](https://about.gitlab.com/handbook/values/#gitlab-values-certification) to become certified in the [CREDIT](https://about.gitlab.com/handbook/values/#credit) values. Check out additional details [here](https://about.gitlab.com/handbook/values/#gitlab-values-certification).

1. [Collaboration](/handbook/values/#collaboration-competency)
1. [Results](/handbook/values/#results-competency)
1. [Efficiency](/handbook/values/#efficiency-competency)
1. [Diversity & Inclusion](/handbook/values/#diversity--inclusion-competency)
1. [Transparency](/handbook/values/#transparency-competency)

### Future Competencies
1. Iteration - TBD
1. Working async: [Why](/company/culture/all-remote/asynchronous/) and [How](/handbook/communication/)
1. Well written artifacts
1. [Single Source of Truth](/handbook/documentation/#documentation-is-the-single-source-of-truth-ssot)
1. [Producing video](/handbook/communication/youtube/)
1. [Handbook first](/handbook/handbook-usage/)
1. [Install GitLab](/install/)
1. [GitLab administration](/ee/administration/)
1. [ROI calculation](/roi/)
