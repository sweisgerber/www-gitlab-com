---
layout: handbook-page-toc
title: "GitLab Recruiting Process Framework for Talent Pools"
---
## Recruiting Process Framework for Talent Pools

**Purpose**: The Recruiting Process Framework for Talent Pools provides guidance on the talent pool process so the recruiting team can efficiently build a talent bench before we need it

## Full Process

### **Step 1: Identify pooling needs**

Anyone on the Recruitment Team can provide sourcing support for talent pools.  The focus should be P3 and P4 Pools. (P0, P1, and P2 are reserved for active REQs) You can **[review active talent pools here](https://gitlab.com/gitlab-com/people-group/recruiting/-/issues/447)**. You may engage with the Recruitment Leads/Managers to verify pool priorities. If the Pool is a priority, use the [Hiring Repo](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/tree/master) to learn more about the Pool profile.

###  **Step 2: Add prospects to talent pool(s) in Greenhouse CRM**

Recruiting Team members should add potential matches to the appropriate Talent Pool in Greenhouse and make use of the appropriate tags to ensure the ability to easily filter prospects for specific job profiles, locations, etc. By default, all new prospects will land in “Prospect Stage” “no stage specified”.

You are encouraged to find top talent using a variety of different sourcing avenues, with LinkedIn and GitLab Talent Community being the most used and efficient ones.

While LinkedIn projects can be used, all approached prospects should be added in Greenhouse as prospects. **Greenhouse is the source of truth.**  Every sourced prospect should have the name of the person who sourced them in the Source field. If a Prospect was sourced by a Recruiting Team Member from the Talent Community, that user will need to [manually update the candidate's source](https://about.gitlab.com/handbook/hiring/greenhouse/#high-level-workflow) information.

There are multiple ways to add the candidates, as follows: 

* [LinkedIn](https://support.greenhouse.io/hc/en-us/articles/204110135-Add-Prospects-to-Greenhouse-via-LinkedIn-RSC-Integration)  
* [Greenhouse plug-in](https://support.greenhouse.io/hc/en-us/articles/201444934-Prospecting-with-Greenhouse-Prospecting-Google-Chrome-Plugin) 
* [Maildrop](https://support.greenhouse.io/hc/en-us/articles/201990630)
* [Enable LinkedIn Recruiter System Connect (RSC)](https://support.greenhouse.io/hc/en-us/articles/115005678103-Enable-LinkedIn-Recruiter-System-Connect-RSC-) so you can export candidates in one click as prospects 

How to add prospects to Talent Pools:
* [From Greenhouse Talent Community Video Walkthrough] (https://drive.google.com/file/d/174J5c0B-O8qHhjILR_FbiteJ1aXO1g_d/view)
* [From LinkedIn Video Walkthrough] (https://drive.google.com/file/d/18EpEpEvlHu3q7qdWcTZMdVtQKftaHZZh/view)

### **Step 3: Qualify Prospects**

If you are helping source for a talent pool please verify the prospects you find are best qualified before reaching out to them. 

The assigned Recruiter(s) to the pool have a 7 day SLA to review prospects in “Prospect Stage” “no stage specified”.  

* If the prospect is *not qualified*, the Recruiter will select the option to “stop considering as a prospect”
*  If the prospect *is qualified*, the assigned Recruiter will change the  “Prospect Stage” to "Qualified" and will ask you to engage the prospect

### **Step 4: Reach out to Prospects**

Once added to the appropriate talent pool and marked as "Qualified", the Recruiting Team member who found the prospect should engage the prospect.  The message should clearly indicate that we do not have immediate opportunities, but are looking to start a proactive relationship.  

Use our [reach out templates](https://docs.google.com/presentation/d/1ySqgLoYnFUGtb7hdywav6iSb_NBPRhfIs6WZlGne6Ww/edit?usp=sharing) or create your own messaging for reaching out to candidates

### **Step 5: Convert Prospects to Nurture**

If the prospect responds with interest, the person who found them will collect the prospects email address, add it to Greenhouse, and will update the “Prospect Stage” in GH from “no stage specified” to “Nurture”.

If the prospect wants to talk with someone at GitLab to learn more about future opportunities, the person who found them will send them the Calandly link of the assigned Recruiter for the Talent Pool so a brief 15-20 "career conversation" call can be set up.  Not all prospects who get moved to “Nurture” have to complete a call. 

The person who completes the "career conversation" call will add brief notes to the prospects profile in GH.

### **Step 6: Nurture Prospects**

Prospects whose “Prospect Stage” in Greenhouse is “Nurture” should receive a monthly message from GitLab.  

<TBD Monthly Nurture Campaign>
